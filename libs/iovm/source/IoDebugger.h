/*#io
Debugger ioDoc(
docCopyright("Steve Dekorte", 2002)
docLicense("BSD revised")
*/

#ifndef IoDebugger_DEFINED
#define IoDebugger_DEFINED 1

#include "IoObject.h"

#ifdef __cplusplus
extern "C" {
#endif

IoObject *IoDebugger_proto(void *state);

#ifdef __cplusplus
}
#endif
#endif
