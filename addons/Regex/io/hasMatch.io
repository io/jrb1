Regex do(
  docSlot("hasMatch", "Returns true if there are any matches or false otherwise.")
  hasMatch := method(currentMatch or nextMatch or false)
)